var APP_DATA = {
  "scenes": [
    {
      "id": "0-daisses",
      "name": "daisses",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1344,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 2.4235506352212033,
          "pitch": 0.10631685964861859,
          "rotation": 0,
          "target": "1-ahja"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -0.06304194451090162,
          "pitch": -0.47404568817085035,
          "title": "Neues Zuhause!",
          "text": ""
        },
        {
          "yaw": 1.047175358261038,
          "pitch": 0.07977972333884153,
          "title": "Nette Nachbarn",
          "text": ""
        }
      ]
    },
    {
      "id": "1-ahja",
      "name": "ahja",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1344,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 2.4238951477928072,
          "pitch": 0.23995177507860888,
          "rotation": 0,
          "target": "2-eugenhilftbestimmt"
        },
        {
          "yaw": -0.09285177868422956,
          "pitch": -0.01760190508162296,
          "rotation": 0,
          "target": "0-daisses"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "2-eugenhilftbestimmt",
      "name": "eugenhilftbestimmt",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1344,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -1.5790718605361107,
          "pitch": 0.1492782050302175,
          "rotation": 0,
          "target": "3-gutegegend"
        },
        {
          "yaw": -0.28660309258121686,
          "pitch": 0.06231774643763721,
          "rotation": 0,
          "target": "1-ahja"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 0.48403149565142556,
          "pitch": -0.11772988221000347,
          "title": "Hier keine Klebzhiks",
          "text": ""
        },
        {
          "yaw": 0.5474203954917218,
          "pitch": -0.10273354880246721,
          "title": "Hier keine Klebzhiks",
          "text": "Text"
        },
        {
          "yaw": 0.5740721569480876,
          "pitch": -0.1446751535165074,
          "title": "Hier keine Klebzhiks",
          "text": "Text"
        },
        {
          "yaw": 0.5127689389613401,
          "pitch": -0.09423576103820608,
          "title": "Hier keine Klebzhiks",
          "text": "Text"
        },
        {
          "yaw": 0.4654520255554999,
          "pitch": -0.10278878636658106,
          "title": "Hier keine Klebzhiks",
          "text": "Text"
        }
      ]
    },
    {
      "id": "3-gutegegend",
      "name": "gutegegend",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1344,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -2.9022257564509033,
          "pitch": 0.18840483026074928,
          "rotation": 0,
          "target": "4-ahhierirgendwo"
        },
        {
          "yaw": -1.1016006277990513,
          "pitch": 0.1608391986708213,
          "rotation": 0,
          "target": "2-eugenhilftbestimmt"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "4-ahhierirgendwo",
      "name": "ahhierirgendwo",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1344,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 1.9703156853556063,
          "pitch": 0.2630765206129091,
          "rotation": 0,
          "target": "5-wodenn"
        },
        {
          "yaw": -0.6503381682581804,
          "pitch": 0.14063898936185026,
          "rotation": 0,
          "target": "3-gutegegend"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 0.45092295135988003,
          "pitch": -0.2958015011937132,
          "title": "hier?",
          "text": '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/9aUowoykENE?rel=0&modestbranding=1&autohide=1&showinfo=0&controls=0" frameborder="0" allowfullscreen></iframe>'
        },
        {
          "yaw": 1.006799452952233,
          "pitch": -0.4764185075179981,
          "title": "Hier?",
          "text": '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/TgqiSBxvdws?rel=0&modestbranding=1&autohide=1&showinfo=0&controls=0" frameborder="0" allowfullscreen></iframe>'
        },
        {
          "yaw": 1.1398901648125825,
          "pitch": -0.07333485633165715,
          "title": "Hier?",
          "text": '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/EBYsx1QWF9A?rel=0&modestbranding=1&autohide=1&showinfo=0&controls=0" frameborder="0" allowfullscreen></iframe>'
        },
        {
          "yaw": 1.6868183933034961,
          "pitch": -0.3042304672971383,
          "title": "hier?",
          "text": ""
        },
        {
          "yaw": 0.15956357988194192,
          "pitch": -0.11018860560391452,
          "title": "hier?",
          "text": ""
        },
        {
          "yaw": 0.7157423037835251,
          "pitch": -0.7730581565385997,
          "title": "Wo ist die WG?",
          "text": ".... hmmmm"
        },
        {
          "yaw": 1.3665535392645989,
          "pitch": -0.23683092664064098,
          "title": "hier?",
          "text": ""
        },
        {
          "yaw": -1.5983425610303925,
          "pitch": -0.09234420187704728,
          "title": "Keine WG",
          "text": ""
        },
        {
          "yaw": 2.6035946736144453,
          "pitch": -0.3388724164721477,
          "title": "Loretanka",
          "text": ""
        },
        {
          "yaw": 2.6378699015751934,
          "pitch": -0.319858771885265,
          "title": "Loretanka&nbsp;",
          "text": ""
        },
        {
          "yaw": -2.5324797079867665,
          "pitch": 0.10582827622101121,
          "title": "Loretanka Zapfsäule",
          "text": ""
        },
        {
          "yaw": -2.48695098177458,
          "pitch": 0.11236849135520366,
          "title": "Loretanka Zapfsäule",
          "text": ""
        },
        {
          "yaw": -3.1144852111279384,
          "pitch": 0.11021160531989338,
          "title": "Loretanka Zapfsäule",
          "text": ""
        },
        {
          "yaw": -1.5519860440082205,
          "pitch": -0.07071264741928296,
          "title": "Keine WG&nbsp;",
          "text": ""
        },
        {
          "yaw": 2.6470111681507653,
          "pitch": -0.3492842406438328,
          "title": "Loredtanka<div><br></div>",
          "text": ""
        }
      ]
    },
    {
      "id": "5-wodenn",
      "name": "wodenn",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1344,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 2.837829866943782,
          "pitch": 0.05636330824268043,
          "rotation": 5.497787143782138,
          "target": "6-hiermussesseinirgendwo"
        },
        {
          "yaw": -1.0124678947651642,
          "pitch": 0.06740376200752607,
          "rotation": 0,
          "target": "4-ahhierirgendwo"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -1.8934984640292285,
          "pitch": -0.2949929349993994,
          "title": "Loretanka",
          "text": ""
        },
        {
          "yaw": -1.8477056564440701,
          "pitch": -0.2680797610774075,
          "title": "Loretanka",
          "text": ""
        },
        {
          "yaw": -1.7956794782946393,
          "pitch": -0.2919652064601692,
          "title": "Loretanka",
          "text": ""
        },
        {
          "yaw": -1.7405088198516676,
          "pitch": -0.2962712952865729,
          "title": "Loretanka",
          "text": ""
        },
        {
          "yaw": -1.8340331172614555,
          "pitch": -0.3168307177752041,
          "title": "Loretanka",
          "text": ""
        },
        {
          "yaw": -1.8080880869168006,
          "pitch": -0.2593951969931947,
          "title": "Loretanka",
          "text": ""
        },
        {
          "yaw": -1.8856117175347809,
          "pitch": -0.2510270146209912,
          "title": "Loretanka",
          "text": ""
        }
      ]
    },
    {
      "id": "6-hiermussesseinirgendwo",
      "name": "hiermussesseinirgendwo",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1344,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 0.02348105538640688,
          "pitch": 0.2833073165082549,
          "rotation": 3.9269908169872414,
          "target": "5-wodenn"
        },
        {
          "yaw": 2.9760169057735144,
          "pitch": 0.11587647174814286,
          "rotation": 0,
          "target": "7-balddaglaubich"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -1.0005459534683432,
          "pitch": -0.030203732611754575,
          "title": "Loretanka",
          "text": ""
        },
        {
          "yaw": -0.9765575216495677,
          "pitch": -0.0031180192573430077,
          "title": "Loretanka",
          "text": ""
        },
        {
          "yaw": -0.9590236218826824,
          "pitch": -0.034358672998031636,
          "title": "Loretanka",
          "text": ""
        },
        {
          "yaw": -0.909404828183046,
          "pitch": -0.016902574294213935,
          "title": "Loretanka",
          "text": ""
        },
        {
          "yaw": -0.9381525807370963,
          "pitch": -0.007581238685876457,
          "title": "Loretanka",
          "text": ""
        },
        {
          "yaw": -0.6827365929581539,
          "pitch": -0.03579970329350779,
          "title": "Loretanka",
          "text": ""
        }
      ]
    },
    {
      "id": "7-balddaglaubich",
      "name": "balddaglaubich",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1344,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.12383616945588649,
          "pitch": 0.29204539217004744,
          "rotation": 6.283185307179586,
          "target": "6-hiermussesseinirgendwo"
        },
        {
          "yaw": 3.0516165243969864,
          "pitch": 0.26700326933752194,
          "rotation": 0,
          "target": "8-dasgehtnichtanders"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "8-dasgehtnichtanders",
      "name": "dasgehtnichtanders",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1344,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.2790466683607402,
          "pitch": 0.16554064460984286,
          "rotation": 6.283185307179586,
          "target": "7-balddaglaubich"
        },
        {
          "yaw": -2.6050299083830737,
          "pitch": 0.10549609758886902,
          "rotation": 0,
          "target": "9-immerich"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "9-immerich",
      "name": "immerich",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1344,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.24177364169518967,
          "pitch": 0.2350007190660257,
          "rotation": 0,
          "target": "8-dasgehtnichtanders"
        },
        {
          "yaw": 2.956841027135985,
          "pitch": 0.46845735869785265,
          "rotation": 0,
          "target": "10-partycheck"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "10-partycheck",
      "name": "partycheck",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1344,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.13999106848679332,
          "pitch": 0.22407814830245698,
          "rotation": 0,
          "target": "9-immerich"
        },
        {
          "yaw": 2.932049146167529,
          "pitch": 0.3255704820159089,
          "rotation": 0,
          "target": "11-obomadaist"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "11-obomadaist",
      "name": "obomadaist",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1344,
      "initialViewParameters": {
        "yaw": 0,
        "pitch": 0,
        "fov": 1.330328938021636
      },
      "linkHotspots": [
        {
          "yaw": -0.15115404704642188,
          "pitch": 0.15047794866952913,
          "rotation": 0,
          "target": "10-partycheck"
        },
        {
          "yaw": 2.895572423047806,
          "pitch": 0.265106657396732,
          "rotation": 0,
          "target": "12-wasmachendiedenn"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -1.9599391354170592,
          "pitch": -0.005802116820428083,
          "title": "Babtschas Löwenkäfig",
          "text": ""
        }
      ]
    },
    {
      "id": "12-wasmachendiedenn",
      "name": "wasmachendiedenn",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1344,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 0.3480216205804929,
          "pitch": 0.2744062704190391,
          "rotation": 0.7853981633974483,
          "target": "11-obomadaist"
        },
        {
          "yaw": 2.850469901186628,
          "pitch": 0.2215591237494099,
          "rotation": 6.283185307179586,
          "target": "13-auchzubabtscha"
        },
        {
          "yaw": -2.633224729788548,
          "pitch": 0.16776826348354845,
          "rotation": 0.7853981633974483,
          "target": "22-gutenmorgen"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "13-auchzubabtscha",
      "name": "auchzubabtscha",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1344,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.011742347368461026,
          "pitch": 0.14228307642653704,
          "rotation": 0,
          "target": "12-wasmachendiedenn"
        },
        {
          "yaw": -3.0965217228753765,
          "pitch": 0.3246331823293289,
          "rotation": 0,
          "target": "14-zubabtscha"
        },
        {
          "yaw": -1.4253073384897128,
          "pitch": 0.1821638560055998,
          "rotation": 0,
          "target": "22-gutenmorgen"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "14-zubabtscha",
      "name": "zubabtscha",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1344,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 2.7980362187935457,
          "pitch": 0.2400402501341219,
          "rotation": 0,
          "target": "15-irgendwaswardochnoch"
        },
        {
          "yaw": -0.19931380754735706,
          "pitch": 0.21810156016698556,
          "rotation": 0,
          "target": "20-manbinichmuede"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "15-irgendwaswardochnoch",
      "name": "irgendwaswardochnoch",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1344,
      "initialViewParameters": {
        "yaw": -2.417449979749996,
        "pitch": 0.10887534965197077,
        "fov": 1.330328938021636
      },
      "linkHotspots": [
        {
          "yaw": -1.9541048023798133,
          "pitch": 0.21442445721168824,
          "rotation": 0,
          "target": "20-manbinichmuede"
        },
        {
          "yaw": -0.24462334009787412,
          "pitch": 0.36037918739220665,
          "rotation": 0,
          "target": "16-hierisschoen"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 2.7587804350393075,
          "pitch": -0.17925700301503866,
          "title": "Oh ich muss die Maklerin&nbsp; anrufen!",
          "planet": true,
          "text": ""
        }
      ]
    },
    {
      "id": "16-hierisschoen",
      "name": "hierisschoen",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1344,
      "initialViewParameters": {
        "yaw": 0.4115121547222138,
        "pitch": -0.033065698504874064,
        "fov": 1.330328938021636
      },
      "linkHotspots": [
        {
          "yaw": 0.8430056253424638,
          "pitch": -0.04206216827152254,
          "rotation": 0,
          "target": "15-irgendwaswardochnoch"
        },
        {
          "yaw": -2.1495612517927807,
          "pitch": 0.23349541521431405,
          "rotation": 0,
          "target": "17-lieberinpark"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "17-lieberinpark",
      "name": "lieberinpark",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1344,
      "initialViewParameters": {
        "yaw": 0.4324987559723361,
        "pitch": 0.055660513813506896,
        "fov": 1.330328938021636
      },
      "linkHotspots": [
        {
          "yaw": 0.4773538238112405,
          "pitch": 0.13287556122102906,
          "rotation": 0,
          "target": "16-hierisschoen"
        },
        {
          "yaw": -2.4421449435313427,
          "pitch": 0.40637919288420044,
          "rotation": 0,
          "target": "18-keinbock"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 0.7413886028945296,
          "pitch": 0.010544030675127658,
          "title": "Oder doch ein bisschen Spazieren gehen...",
          "planet": true,
          "text": ""
        }
      ]
    },
    {
      "id": "18-keinbock",
      "name": "keinbock",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1344,
      "initialViewParameters": {
        "yaw": -0.7436233276379234,
        "pitch": -0.02840197796244759,
        "fov": 1.330328938021636
      },
      "linkHotspots": [
        {
          "yaw": 1.8627351706517157,
          "pitch": 0.10787382546054936,
          "rotation": 0,
          "target": "17-lieberinpark"
        },
        {
          "yaw": -2.983111758129528,
          "pitch": 0.19492780594729808,
          "rotation": 0,
          "target": "19-gleichda"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -1.856811546943744,
          "pitch": 0.23140433021636753,
          "planet" : true,
          "title": "Ist Flo schon da?",
          "text": "<span style=\"font-size: 16px; background-color: rgba(103, 115, 131, 0.8);\">Arbeit fühlt sich echt manchmal wie Spielen an...</span>"
        }
      ]
    },
    {
      "id": "19-gleichda",
      "name": "gleichda",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1344,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.16953688217443386,
          "pitch": 0.2943707060826277,
          "rotation": 0,
          "target": "18-keinbock"
        },
        {
          "yaw": 3.0246058831917413,
          "pitch": 0.21366964464567317,
          "rotation": 0,
          "target": "20-manbinichmuede"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "20-manbinichmuede",
      "name": "manbinichmuede",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1344,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.3705092452456995,
          "pitch": 0.31631030003467586,
          "rotation": 0,
          "target": "19-gleichda"
        },
        {
          "yaw": -2.631709189355494,
          "pitch": 0.2461822359774537,
          "rotation": 0,
          "target": "21-aufzuarbeit"
        },
        {
          "yaw": 1.054802230736776,
          "pitch": 0.23109529362890413,
          "rotation": 0,
          "target": "15-irgendwaswardochnoch"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "21-aufzuarbeit",
      "name": "aufzuarbeit",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1344,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 2.2608115907474495,
          "pitch": 0.2469195505806212,
          "rotation": 0,
          "target": "22-gutenmorgen"
        },
        {
          "yaw": -0.24192784337559914,
          "pitch": 0.2832550860428853,
          "rotation": 0,
          "target": "20-manbinichmuede"
        },
        {
          "yaw": 3.056894314886595,
          "pitch": 0.1791951576092785,
          "rotation": 0,
          "target": "13-auchzubabtscha"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -1.7767652245176997,
          "pitch": -0.05546277475991879,
          "title": "Worauf warten die alle?",
          "text": "hm egal, ich muss zur Arbeit"
        }
      ]
    },
    {
      "id": "22-gutenmorgen",
      "name": "gutenmorgen",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1344,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.21593336556187026,
          "pitch": 0.4022450862378122,
          "rotation": 0,
          "target": "21-aufzuarbeit"
        },
        {
          "yaw": -1.6206521560004532,
          "pitch": 0.24408079819048467,
          "rotation": 0,
          "target": "13-auchzubabtscha"
        }
      ],
      "infoHotspots": []
    }
  ],
  "name": "Project Title",
  "settings": {
    "mouseViewMode": "qtvr",
    "autorotateEnabled": false,
    "fullscreenButton": false,
    "viewControlButtons": false
  }
};
